# Create_new_IAM_user_with_existing_policy
# Combined with the current list of employees in the database or excel and python script, 
# we can monitor and automatically grant access to company resources for each employee on a daily basis. 
# Access is granted daily and is secured by the "AWS Role" and "AWS Policy". 
 
# What's included: 
# Create new user in AWS 
# Creating new Role 
# Add existing role to a new created user 
# Create web_services / menagment console access for new user 
# Create CLI/SDK access for new user 


# What needs to be set:
policy_name = "StrictEC2"
local_UTC_time_difference = -2
user_work_start_time = "05:00:00"
user_work_end_time = "17:00:00"
user_work_place_ip_addresses = ["178.7.55.70"]
user_department = ""   # optional
# IN FILE aws_iam_policy YOU NEED TO SET POLICY THAT YOU WANT!

#How to use:
#terraform init
#terraform apply --auto-approve
#terraform output -json  (for hidden variables)


# isues
#From time to time it will create new password that is not correct with aws policy. 
#Then you need to destroy and apply again 