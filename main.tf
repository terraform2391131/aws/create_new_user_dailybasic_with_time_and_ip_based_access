data "aws_caller_identity" "current" {}

# create new user
resource "aws_iam_user" "JohnDoe_user" {
  name = join("_", [var.user_name, var.policy_name])
  path = join("", ["/", var.user_department]) # group user by path
}

resource "aws_iam_access_key" "JohnDoe_access_key" {
  user = aws_iam_user.JohnDoe_user.name
}

# create new role
resource "aws_iam_role" "JohnDoe_role" {
  name = "StrictDay2DayCompanyPolicy"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          AWS = join("", ["arn:aws:iam::", data.aws_caller_identity.current.account_id, ":user/", var.user_name, "_", var.policy_name])
          #"arn:aws:iam::162373986151:user/JohnDoe_StrictEC2"
        },
        Effect = "Allow",
        Sid    = ""
      }
    ]
  })

  max_session_duration = 43200  # session max. duration
  # 3600  - 1h
  # 43200 - 12h
}

# adding new role to user
resource "aws_iam_policy" "strict_policy" {
  name        = "StrictPolicy"
  description = "Strict EC2 day2day company policy"
  
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "ec2:Describe*",
        Resource = "*"
        Condition = {
          IpAddress = {
            "aws:SourceIp" = var.user_work_place_ip_addresses
          },
          DateGreaterThan = {
            # "aws:CurrentTime" : "2023-08-23T05:00:00Z"
            "aws:CurrentTime" : local.DateGreaterThan
          },
          DateLessThan = {
            #"aws:CurrentTime" : "2023-08-23T06:00:00Z"
            "aws:CurrentTime" : local.DateLessThan
          },
          # Bool = {
          #   "aws:MultiFactorAuthPresent" = "true"
          # }
        }
      }
    ]
  })
}


# add policy to user (not policy to role!)
resource "aws_iam_user_policy_attachment" "example_attach" {
  user       = aws_iam_user.JohnDoe_user.name
  policy_arn = aws_iam_policy.strict_policy.arn
}

# create access to AWS Management Console
resource "aws_iam_user_login_profile" "new_user_profile" {
  user = aws_iam_user.JohnDoe_user.name
  password_length = 16
}
