# Create_new_IAM_user_with_existing_policy
Combined with the current list of employees in the database or excel and python script,<br>
we can monitor and automatically grant access to company resources for each employee on a daily basis.<br>
Access is granted daily and is secured by the "AWS Role" and "AWS Policy".<br>
<br>
What's included:<br>
Create new user in AWS<br>
Creating new Role<br>
Add existing role to a new created user<br>
Create web_services / menagment console access for new user<br>
Create CLI/SDK access for new user<br>

# Edit settings in "terraform.tfvars" file ! read instructions there

#How to use:<br>
terraform init<br>
terraform apply --auto-approve<br>
terraform output -json  (for hidden variables)<br>

# isues
From time to time it will create new password that is not correct with aws policy.<br>
Then you need to destroy and apply again<br>

