output "aws_web_services_account_id" { # to login on signin.aws.amazon.com
  value = data.aws_caller_identity.current.account_id
}

output "aws_web_services_IAM_user_name" { # to login on signin.aws.amazon.com
  value = aws_iam_user.JohnDoe_user.name
}

output "aws_web_services_initial_password" { # to login on signin.aws.amazon.com
  value = aws_iam_user_login_profile.new_user_profile.password
}

output "aws_web_services_initial_encrypted_password" { # will show this or aws_web_services_initial_password
  value = aws_iam_user_login_profile.new_user_profile.encrypted_password
}

output "cli_access_key_id" { #SDK/CLI AWS
  value = aws_iam_access_key.JohnDoe_access_key.id
}

output "cli_secret_access_key" { #SDK/CLI AWS
  sensitive = true
  value = aws_iam_access_key.JohnDoe_access_key.secret
}

output "cli_encrypted_secret_access_key" { #SDK/CLI AWS will show this or cli_secret_access_key
  value = aws_iam_access_key.JohnDoe_access_key.encrypted_secret
}

output "aws_iam_smtp_password_v4" {  # for Amazon Simple Email Service (Amazon SES).
  sensitive = true
  value = aws_iam_access_key.JohnDoe_access_key.ses_smtp_password_v4
}