variable "user_name"{
    description = "Please enter new user name"
    type = string
    default = "JohnDoe"
}

variable "policy_name" {
    description = "Your policy name"
    type = string
    default = "StrictPolicy1"
}

variable "user_department" {
    description = "Group users on AWS by department"
    type = string
    default = ""
}


variable "local_UTC_time_difference" {
    description = "Your local time differecne against UTC. example: current utc:06:00, current local:08:00 ... diference = -2"
    type = string
    default = "0"
}

variable "user_work_start_time" {
    description = "HH:mm:ss time format, work start from"
    type = string
    default = "07:00:00"
}

variable "user_work_end_time" {
    description = "HH:mm:ss time format, work end"
    type = string
    default = "17:00:00"
}

variable "user_work_place_ip_addresses" {
    description = "user work places ip addresses list ['192.0.2.0/24', '203.0.113.0/24']"
    type = list
}


locals {
  current_date_utc = formatdate("YYYY-MM-DD", timestamp())

  user_work_start_datetime_formatted1 = join("T", [local.current_date_utc, var.user_work_start_time])
  user_work_start_datetime_formatted2 = join("", [local.user_work_start_datetime_formatted1, "Z"])
  DateGreaterThan = timeadd(local.user_work_start_datetime_formatted2, "${var.local_UTC_time_difference}h")

  user_work_end_datetime_formatted1 = join("T", [local.current_date_utc, var.user_work_end_time])
  user_work_end_datetime_formatted2 = join("", [local.user_work_end_datetime_formatted1, "Z"])
  DateLessThan = timeadd(local.user_work_end_datetime_formatted2, "${var.local_UTC_time_difference}h")

}
